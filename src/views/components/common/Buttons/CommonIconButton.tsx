import { IconButton as MatUIIconButton } from "@material-ui/core";
import * as React from "react";

interface ButtonProps {
  children?: React.ReactNode | React.ReactNode[];
  onClick: () => void;
}

export const CommonIconButton = (props: ButtonProps) => {
  const { children, onClick } = props;

  return (
    <MatUIIconButton onClick={() => onClick()}>{children}</MatUIIconButton>
  );
};
