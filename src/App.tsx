import * as React from "react";

import { LandingPage } from "./views/pages/landing-page/LandingPage";

function App() {
  return <LandingPage />;
}

export default App;
